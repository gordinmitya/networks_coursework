<?
	
include $_SERVER['DOCUMENT_ROOT'].'/App.php';

function writeResult($user, $attempts, $time) {
	$m = Mysql::get();
    $stmt = $m->prepare("INSERT INTO `results`(`user_id`, `time`, `attempts`) 
    	VALUES (?,?,?)");
    $stmt->bind_param('iii', $user->id, $time, $attempts);
    $stmt->execute();
    $stmt->close();
    Mysql::close($m);
}

function codeDecode($value) {
	return $value ^ 1050;
}

$token = $_REQUEST['token'] ?? '';
$u = new User($token);

$inValue = intval($_REQUEST['inValue'] ?? 0);
$startTime = intval($_REQUEST['startTime'] ?? 0);
$encodedAttempts = intval($_REQUEST['encodedAttempts'] ?? 0);
$attempts = codeDecode($encodedAttempts);
$encodedSecret = intval($_REQUEST['secret'] ?? 0);
$secret = codeDecode($encodedSecret);

$status = '';

//=>new game
if ($encodedSecret == 0) {
	$startTime = time();
	$attempts = 0;
	$status = 'NEW_GAME';
	$secret = rand(1,100);
} else {
	if (($inValue == $secret)
		&& ($attempts <= 7)) {
		$status = 'WIN';
		writeResult($u, $attempts, time()-$startTime);
	} else {
		if ($attempts > 7) {
			$status = 'LOSE';
		} else {
			$attempts += 1;
			if ($secret>$inValue) {
				$status = 'LARGER';
			} else {
				$status = 'LESS';
			}
		}
	}
}

echo json_encode([
	'status' => $status,
	'encodedAttempts' => codeDecode($attempts),
	'attempts' => $attempts,
	'secret' => codeDecode($secret),
	'startTime' => $startTime
]);