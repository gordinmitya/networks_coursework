<?
//SELECT DISTINCT user_id, time, attempts FROM `results` ORDER by attempts DESC, time DESC;
include $_SERVER['DOCUMENT_ROOT'].'/App.php';

$m = Mysql::get();
$stmt = $m->prepare("SELECT DISTINCT user_id, name, time, attempts FROM `results` r, `users` u WHERE r.user_id = u.id ORDER by attempts DESC, time DESC ");
$stmt->execute();
$res = $stmt->get_result();
$json = [];
while ($r = $res->fetch_assoc()) {
	$json[]=[
		'name'=>$r['name'],
		'attempts'=>$r['attempts'],
		'time'=>$r['time']
	];
}
$stmt->close();
Mysql::close($m);		

echo json_encode($json);