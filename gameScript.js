var secret = 0;
var encodedAttempts = 0;
var startTime = 0;

function makeRequest(inValue) {
	var token = localStorage.getItem('token')
	if (token == undefined) { token = '' }
	$.getJSON('/api/game.php?token='+token
		+'&inValue='+inValue
		+'&startTime='+startTime
		+'&encodedAttempts='+encodedAttempts
		+'&secret='+secret
		, parseAnswer)
}
function parseAnswer(data) {
	console.log(data)
	switch (data.status) {
	case 'NEW_GAME':
		$('#winImage').css('display', 'none')
		$('#loseImage').css('display', 'none')
		$('#message').html("Время пошло!")
		availableGame(true)
		break
	case 'WIN':
		$('#winImage').css('display', 'block')
		$('#message').html("Победа")
		availableGame(false)
		break
	case 'LOSE':
		$('#loseImage').css('display', 'block')
		$('#message').html("Проигрыш")
		availableGame(false)
		break
	case 'LARGER':
		$('#message').html("Загаданное число больше")
		break
	case 'LESS':
		$('#message').html("Загаданное число меньше")
		break
	}

	attempts = data.attempts
	encodedAttempts = data.encodedAttempts
	secret = data.secret
	startTime = data.startTime

	if (data.status == 'LARGER' || data.status == 'LESS') {
		$('#attempts').parent().css('display', 'block')
		$('#attempts').html(7-attempts);
	} else {
		$('#attempts').parent().css('display', 'none')
	}

	$("#game input[name='inValue']")[0].value = ''
}
function availableGame(value) {
	$('#send').prop("disabled", !value)
	$("#game input[name='inValue']").prop("disabled", !value)
}

function sendClick() {
	var value = parseInt($("#game input[name='inValue']")[0].value)
	if (value<1 || value>100 || isNaN(value)) {
		$('#message').html("Не корректный ввод")
	} else {
		makeRequest(value);
	}
}
function restartClick() {
	secret = 0;
	makeRequest(0);
}

function refreshResults() {
	$("#results tr.value").remove();
	$.getJSON('/api/results.php', function (data) {
		console.log(data)
		for (var i = 0; i < data.length; i++) {
			$("#results tr:last").before(
				'<tr class="value">'
				+'<td>'+data[i].name+'</td>'
				+'<td>'+data[i].attempts+'</td>'
				+'<td>'+data[i].time+'</td>'
				+'</tr>'
			);
		}
	});
}












