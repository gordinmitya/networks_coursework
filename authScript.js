function switchToHello (username) {
	hideAuthTables()
	$('#helloTable').css('display', 'table')
	$('#helloTable #username').html(username);
}
function switchToEn () {
	hideAuthTables()
	$('#enterTable').css('display', 'table')
}
function switchToReg () {
	hideAuthTables()
	$('#regTable').css('display', 'table')
}
function hideAuthTables() {
	$('form.auth table').css('display', 'none')
}

function addAnswer (elem){
	var table = $(elem).parents("table")
	var template = $("#newAnswer_template").html()
	table.find('tr:last').before(template)
}
function deleteRow(elem) {
	$(elem).parents("tr").remove();
}
function addQuestion(elem) {
	var template = $('#newQuestion_template').html()
	$(elem).before(template)
}
function deleteQuestion(elem) {
	$(elem).parents(".newQuestion").remove();
}

function checkAuth() {
	var token = localStorage.getItem('token')
	if (token == undefined) {
		switchToEn()
		return
	}
	$.getJSON('/api/auth.php?token='+token, function(data) {
		if (data.type != 'anonym') {
			switchToHello(data.name)
		} else {
			switchToEn()
		}
	})
}
function doLogin() {
	var name = $("#enterTable input[name='username']")[0].value
	var pass = $("#enterTable input[name='password']")[0].value
	$.getJSON('/api/login.php?login='+name+'&pass='+pass, function(data) {
		if (data.type != 'anonym') {
			switchToHello(data.name)
			localStorage.setItem('token', data.token)
		} else {
			$("#enterTable tr.error").css('display', 'table-row')
			$("#enterTable tr.error td").html('Не верный логин/пароль')
		}
	})
}
function doRegister() {
	var name = $("#regTable input[name='username']")[0].value
	var pass = $("#regTable input[name='password']")[0].value
	var pass_again = $("#regTable input[name='password_again']")[0].value
	var sex = $("#regTable select[name='sex']")[0].value
	var age = $("#regTable input[name='age']")[0].value
	if (pass != pass_again) {
		$("#regTable tr.error").css('display', 'table-row')
		$("#regTable tr.error td").html('Пароли не совпадают')
		return
	}
	$.getJSON('/api/register.php?login='+name+'&pass='+pass
		+'&sex'+sex+'&age='+age, function(data) {
		if (data.type != 'anonym') {
			switchToHello(data.name)
			localStorage.setItem('token', data.token)
		} else {
			$("#regTable tr.error").css('display', 'table-row')
			$("#regTable tr.error td").html('Не корректно заполнены поля')
		}
	})
}
function logout() {
	localStorage.removeItem('token')
	switchToEn()
}